package com.iwec.rest.example.repository;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.iwec.rest.example.model.Player;


@Component
public class PlayerRepository {
	private static final Map<Integer, Player> players = new TreeMap<>();

	@PostConstruct
	public void initData() {

		Player player = new Player();
		player.setId(1);
		player.setFirstName("Kyle");
		player.setLastName("Irving");
		player.setRole("Playmaker");
		players.put(player.getId(), player);

		player = new Player();
		player.setId(2);
		player.setFirstName("Kobe");
		player.setLastName("Bryant");
		player.setRole("Shooting Guard");
		players.put(player.getId(), player);
		
		
		player = new Player();
		player.setId(3);
		player.setFirstName("Joel");
		player.setLastName("Embid");
		player.setRole("Center");
		players.put(player.getId(), player);

		player = new Player();
		player.setId(4);
		player.setFirstName("Kyle");
		player.setLastName("Kuzma");
		player.setRole("Shooting Guard");
		players.put(player.getId(), player);

		
	}

	public Player findById(Integer id) {
		return (id == null) ? null : players.get(id);
	}

	public List<Player> getAllPlayersOrderedById() {
		List<Player> playerList = new ArrayList<>(players.values());
		return playerList;
	}

	public Integer saveOrUpdate(Player player) {
		Integer key = (player == null || player.getId() == null)
				? players.size() + 1
				: player.getId();
		player.setId(key);
		players.put(key, player);
		return player.getId();
	}

	public boolean delete(Integer id) {
		Player player = players.remove(id);
		return (player != null) ? true : false;
	}
}