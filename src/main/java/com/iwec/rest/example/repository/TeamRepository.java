package com.iwec.rest.example.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.iwec.rest.example.model.Team;

@Component
public class TeamRepository {

	private static final Map<Integer, Team> teams = new TreeMap<>();

	@PostConstruct
	public void initData() {

		Team team = new Team();
		team.setId(1);
		team.setName("Brooklyn Nets");
		teams.put(team.getId(), team);

		team = new Team();
		team.setId(2);
		team.setName("Los Angele Lakers");
		teams.put(team.getId(), team);

		team = new Team();
		team.setId(3);
		team.setName("Philadelphia 76ers");
		teams.put(team.getId(), team);

		team = new Team();
		team.setId(4);
		team.setName("Los Angeles Clipers");
		teams.put(team.getId(), team);

	}

	public Team findTeambyId(Integer id) {
		return (id == null) ? null : teams.get(id);
	}

	public List<Team> getAllTeamsOrderedById() {
		List<Team> teamList = new ArrayList<>(teams.values());
		return teamList;
	}

	public Integer saveorupdate(Team team) {
		Integer key = (team == null || team.getId() == null) ? teams.size() + 1 : team.getId();
		team.setId(key);
		teams.put(key, team);
		return team.getId();
	}

	public boolean delete(Integer id) {
		Team team = new Team();
		return (team != null) ? true : false;
	}
}
