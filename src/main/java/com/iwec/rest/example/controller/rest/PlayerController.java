package com.iwec.rest.example.controller.rest;

import static com.iwec.rest.example.util.Constants.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.iwec.rest.example.model.Player;
import com.iwec.rest.example.repository.PlayerRepository;


@RestController
@RequestMapping("v1/rest/players")
public class PlayerController {

	@Autowired
	private PlayerRepository repository;

	@RequestMapping(method = RequestMethod.GET, produces = PRODUCES)
	@ResponseBody
	public List<Player> getAllPlayersOrderedById() {
		return repository.getAllPlayersOrderedById();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Player> getById(
			@PathVariable(value = "id") Integer id) {
		Player player = repository.findById(id);
		HttpStatus status = player != null
				? HttpStatus.OK
				: HttpStatus.NOT_FOUND;
		return new ResponseEntity<Player>(player, status);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = CONSUMES)
	@ResponseStatus(HttpStatus.CREATED)
	public Integer insert(@RequestBody Player player) {
		return repository.saveOrUpdate(player);
	}

	@RequestMapping(method = RequestMethod.PUT, consumes = CONSUMES)
	@ResponseStatus(HttpStatus.OK)
	public Integer update(@RequestBody Player player) {
		// TODO from repository return new created object,
		// and check it as with GET and POST
		return repository.saveOrUpdate(player);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	
	public HttpStatus delete(@PathVariable(value = "id") Integer id) {
		boolean isDeleted = repository.delete(id);
		System.err.println("######" + isDeleted);
		return (isDeleted) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
//		return new ResponseEntity<Student>(student, status);

	}
}