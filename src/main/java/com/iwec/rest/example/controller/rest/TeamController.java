package com.iwec.rest.example.controller.rest;

import static com.iwec.rest.example.util.Constants.CONSUMES;
import static com.iwec.rest.example.util.Constants.PRODUCES;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.iwec.rest.example.model.Team;
import com.iwec.rest.example.repository.TeamRepository;

@RestController
@RequestMapping("v1/rest/teams")
public class TeamController {

	@Autowired
	private TeamRepository repository;

	@RequestMapping(method = RequestMethod.GET, produces = PRODUCES)
	@ResponseBody
	public List<Team> getAllTeamsOrderedById() {
		return repository.getAllTeamsOrderedById();
	}

	@RequestMapping(value = "{/id)", method = RequestMethod.GET)
	public ResponseEntity<Team> getById(@PathVariable(value = "id") Integer Id) {
		Team team = repository.findTeambyId(Id);
		HttpStatus status = team != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
		return new ResponseEntity<Team>(team, status);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = CONSUMES)
	@ResponseStatus(HttpStatus.CREATED)
	public Integer insert(@RequestBody Team team) {
		return repository.saveorupdate(team);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)

	public HttpStatus delete(@PathVariable(value = "id") Integer id) {
		boolean isDeleted = repository.delete(id);
		System.err.println("######" + isDeleted);
		return (isDeleted) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
	}
}
